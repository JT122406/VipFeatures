![Header](https://i.imgur.com/P6iOg3f.png)

[![Documentation](https://i.imgur.com/7QDbrIS.png)](https://gitlab.com/andrei1058/VipFeatures/-/wikis/home) [![Report a Bug](https://i.imgur.com/Z1qOYLC.png)](https://gitlab.com/andrei1058/VipFeatures/issues) [![API](https://i.imgur.com/JfMTMMc.png)](http://javadocs.andrei1058.com/VipFeatures/) [![Discord](https://i.imgur.com/yBySzkU.png)](https://discord.gg/XdJfN2X)  

A Minecraft (spigot) Plugin that provide donor perks. You can sync player preferences around your network by connecting it to a MySQL database, but don't worry, if you have a small server you can use the default SQLite local database. Each perk has its own permission so you can customise it for multiple donor ranks. For configuration help or details check the ![wiki](https://gitlab.com/andrei1058/VipFeatures/-/wikis/home).  
If you are a developer you can easily integrate it in your mini-game. Give a look at the ![Getting Started](https://gitlab.com/andrei1058/VipFeatures/-/wikis/Getting-Started) page. You can always contact me in ![Discord](https://discord.gg/XdJfN2X) if you need help.

**DOWNLOAD**
- [Latest release](https://www.spigotmc.org/resources/71152)
- [Development builds](https://ci.codemc.io/job/andrei1058/job/VipFeatures/)


**Maven Repo**
```xml
<repository>
  <id>andrei1058-releases</id>
  <url>http://repo.andrei1058.com/releases</url>
</repository>

```
**Maven Dependency**
```xml
<dependency>
  <groupId>com.andrei1058.vipfeatures</groupId>
  <artifactId>vipfeatures-api</artifactId>
  <version>[1.3.0,)</version>
</dependency>

```

[![Codacy Badge](https://app.codacy.com/project/badge/Grade/81e46c6a9af54503bcad130884b87a13)](https://www.codacy.com/gl/andrei1058/VipFeatures/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=andrei1058/VipFeatures&amp;utm_campaign=Badge_Grade) ![Servers](https://img.shields.io/bstats/servers/8977) ![Version](https://img.shields.io/spiget/version/71152) [![Chat](https://img.shields.io/discord/201345265821679617)](https://discord.gg/XdJfN2X) ![Size](https://img.shields.io/spiget/download-size/71152) ![Downloads](https://img.shields.io/spiget/downloads/71152) ![Rating](https://img.shields.io/spiget/rating/71152) ![Versions](https://img.shields.io/spiget/tested-versions/71152)