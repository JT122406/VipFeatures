package com.andrei1058.vipfeatures.api;

public enum TrailType {
    NONE(null, ""), FIRE("FLAME", "vipfeatures.tails.fire"),
    SLIME("SLIME", "vipfeatures.trails.slime"),
    WATER("WATER_SPLASH", "vipfeatures.trails.water"),
    NOTES("NOTE", "vipfeatures.trails.notes"),
    CRYSTAL("CRIT_MAGIC", "vipfeatures.trails.crystal");

    private final String particle, permission;

    TrailType(String particle, String permission) {
        this.particle = particle;
        this.permission = permission;

    }

    public String getPermission() {
        return permission;
    }

    public String getParticle() {
        return particle;
    }
}
