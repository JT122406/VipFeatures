package com.andrei1058.vipfeatures.api;

import org.bukkit.plugin.Plugin;

public class MiniGameAlreadyRegistered extends Throwable {

    public MiniGameAlreadyRegistered(Plugin plugin) {
        super("Cannot register mini-game integration for: " + plugin.getName() + ". Mini-game already registered.");
    }
}
