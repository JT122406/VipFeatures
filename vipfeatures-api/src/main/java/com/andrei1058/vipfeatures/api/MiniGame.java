package com.andrei1058.vipfeatures.api;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public abstract class MiniGame {

    private final Plugin owner;

    /**
     * Create a mini game integration.
     *
     * @param plugin your mini game instance.
     */
    public MiniGame(Plugin plugin) {
        this.owner = plugin;
    }

    /**
     * Check if a player is playing.
     * He won't be able to interact with perks of is playing
     */
    public abstract boolean isPlaying(Player p);

    /**
     * Check if a mini-game has boosters support
     */
    public abstract boolean hasBoosters();

    /**
     * Get mini-game display name
     */
    public abstract String getDisplayName();

    public Plugin getOwner() {
        return owner;
    }
}
