package com.andrei1058.vipfeatures.listeners;

import com.andrei1058.vipfeatures.VipFeatures;
import com.andrei1058.vipfeatures.api.*;
import com.andrei1058.vipfeatures.configuration.Language;
import com.andrei1058.vipfeatures.configuration.Messages;
import com.andrei1058.vipfeatures.configuration.Permissions;
import com.andrei1058.vipfeatures.customgui.CustomGUI;
import com.andrei1058.vipfeatures.items.ItemCreator;
import com.andrei1058.vipfeatures.perks.BoostersManager;
import com.andrei1058.vipfeatures.perks.ParticleManager;
import com.andrei1058.vipfeatures.perks.SpellsManager;
import com.andrei1058.vipfeatures.perks.TrailsManager;
import com.andrei1058.vipfeatures.tasks.ParticlesTask;
import com.andrei1058.vipfeatures.tasks.TrailsTask;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitTask;

public class GeneralListeners implements Listener {

    @EventHandler
    // Used to open main GUI
    public void onInteract(PlayerInteractEvent e) {
        final ItemStack i = VipFeatures.getItemStackSupport().getInHand(e.getPlayer());
        if (i == null) return;
        if (i.getType() == Material.AIR) return;
        for (MiniGame m : VipFeatures.getMiniGames()) {
            if (m.isPlaying(e.getPlayer())) return;
        }
        String tag = VipFeatures.getItemStackSupport().getTag(i, VipFeatures.INTERACT_TAG);
        if (tag != null) {
            if (tag.equalsIgnoreCase(ItemCreator.ItemType.VIPFEATURES_MAIN_GUI_OPENER.toString())) {
                if (!e.isCancelled()) e.setCancelled(true);
                Language.getPlayerLanguage(e.getPlayer()).openGUI("mainGUI", e.getPlayer(), Permissions.PERMISSION_RECEIVE_AND_OPEN_MAIN_GUI);
            }
        }
    }

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        if (e.getWhoClicked() == null) return;
        if (!(e.getWhoClicked() instanceof Player)) return;

        final Player player = (Player) e.getWhoClicked();
        Language l = Language.getPlayerLanguage(player);

        for (CustomGUI cg : l.getLangGUIs()) {
            if (l.m(cg.getName() + ".name").equalsIgnoreCase(e.getInventory().getTitle())) {
                if (!e.isCancelled()) e.setCancelled(true);
                switch (cg.getName()) {
                    case "mainGUI":
                        for (String s : VipFeatures.mainGUI.getYml().getConfigurationSection("mainGUI").getKeys(false)) {
                            if (s.equalsIgnoreCase("invSize")) continue;
                            if (VipFeatures.mainGUI.getYml().get("mainGUI." + s + ".slot") == null) continue;
                            if (VipFeatures.mainGUI.getInt("mainGUI." + s + ".slot") == e.getSlot()) {
                                Language.getPlayerLanguage(player).openGUI(s, player, "");
                                return;
                            }
                        }
                        break;
                    case "trailsGUI":
                        for (String s : VipFeatures.trailsGUI.getYml().getConfigurationSection("trailsGUI").getKeys(false)) {
                            if (VipFeatures.trailsGUI.getYml().get("trailsGUI." + s + ".slot") == null) continue;
                            if (VipFeatures.trailsGUI.getInt("trailsGUI." + s + ".slot") == e.getSlot()) {
                                if (s.equalsIgnoreCase("backItem")) {
                                    Language.getPlayerLanguage(player).openGUI("mainGUI", player, "");
                                    return;
                                }
                                TrailType current = TrailsManager.getINSTANCE().getPlayerTrails(player);
                                TrailType selected;
                                try {
                                    selected = TrailType.valueOf(s);
                                } catch (Exception exception) {
                                    player.closeInventory();
                                    // invalid trail
                                    return;
                                }

                                String trailName;
                                if (current.equals(selected)) {
                                    // deselect
                                    selected = TrailType.NONE;
                                    trailName = Language.getMsg(player, Messages.NONE_MEANING);
                                } else {
                                    trailName = ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName());
                                }
                                TrailType finalSelected = selected;
                                Bukkit.getScheduler().runTaskAsynchronously(VipFeatures.plugin,
                                        () -> VipFeatures.databaseAdapter.setSelectedTrails(player.getUniqueId(), finalSelected));
                                player.sendMessage(Language.getMsg(player, Messages.TRAIL_SELECTED_MESSAGE).replace("{name}", trailName));
                                e.getWhoClicked().closeInventory();
                                TrailsManager.getINSTANCE().setPlayerTrails(player, selected);
                                return;
                            }
                        }
                        break;
                    case "spellsGUI":
                        for (String s : VipFeatures.spellsGUI.getYml().getConfigurationSection("spellsGUI").getKeys(false)) {
                            if (VipFeatures.spellsGUI.getYml().get("spellsGUI." + s + ".slot") == null) continue;
                            if (VipFeatures.spellsGUI.getInt("spellsGUI." + s + ".slot") == e.getSlot()) {
                                if (s.equalsIgnoreCase("backItem")) {
                                    Language.getPlayerLanguage(player).openGUI("mainGUI", player, "");
                                    return;
                                }

                                SpellType current = SpellsManager.getINSTANCE().getPlayerSpells(player);
                                SpellType selected;
                                try {
                                    selected = SpellType.valueOf(s);
                                } catch (Exception exception) {
                                    player.closeInventory();
                                    // invalid trail
                                    return;
                                }

                                String spellName;
                                if (current.equals(selected)) {
                                    // deselect
                                    selected = SpellType.NONE;
                                    spellName = Language.getMsg(player, Messages.NONE_MEANING);
                                } else {
                                    spellName = ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName());
                                }
                                SpellType finalSelected = selected;
                                Bukkit.getScheduler().runTaskAsynchronously(VipFeatures.plugin,
                                        () -> VipFeatures.databaseAdapter.setSelectedSpells(player.getUniqueId(), finalSelected));
                                player.sendMessage(Language.getMsg(player, Messages.SPELL_SELECTED_MESSAGE).replace("{name}", spellName));
                                e.getWhoClicked().closeInventory();
                                SpellsManager.getINSTANCE().setPlayerSpells(player, selected);
                                return;
                            }
                        }
                        break;
                    case "particlesGUI":
                        for (String s : VipFeatures.particlesGUI.getYml().getConfigurationSection("particlesGUI").getKeys(false)) {
                            if (VipFeatures.particlesGUI.getYml().get("particlesGUI." + s + ".slot") == null) continue;
                            if (VipFeatures.particlesGUI.getInt("particlesGUI." + s + ".slot") == e.getSlot()) {
                                if (s.equalsIgnoreCase("backItem")) {
                                    Language.getPlayerLanguage(player).openGUI("mainGUI", player, "");
                                    return;
                                }

                                ParticleType current = ParticleManager.getINSTANCE().getPlayerParticles(player);
                                ParticleType selected;
                                try {
                                    selected = ParticleType.valueOf(s);
                                } catch (Exception exception) {
                                    player.closeInventory();
                                    // invalid trail
                                    return;
                                }

                                String displayName;
                                if (current.equals(selected)) {
                                    // deselect
                                    selected = ParticleType.NONE;
                                    displayName = Language.getMsg(player, Messages.NONE_MEANING);
                                } else {
                                    displayName = ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName());
                                }
                                ParticleType finalSelected = selected;
                                Bukkit.getScheduler().runTaskAsynchronously(VipFeatures.plugin,
                                        () -> VipFeatures.databaseAdapter.setSelectedParticles(player.getUniqueId(), finalSelected));
                                player.sendMessage(Language.getMsg(player, Messages.PARTICLES_SELECTED_MESSAGE).replace("{name}", displayName));
                                e.getWhoClicked().closeInventory();
                                ParticleManager.getINSTANCE().setPlayerParticles(player, selected);
                                return;
                            }
                        }
                        break;
                    case "boostersGUI":
                        for (String s : VipFeatures.boostersGUI.getYml().getConfigurationSection("boostersGUI").getKeys(false)) {
                            if (VipFeatures.boostersGUI.getYml().get("boostersGUI." + s + ".slot") == null) continue;
                            if (VipFeatures.boostersGUI.getInt("boostersGUI." + s + ".slot") == e.getSlot()) {
                                if (s.equalsIgnoreCase("backItem")) {
                                    Language.getPlayerLanguage(player).openGUI("mainGUI", player, "");
                                    return;
                                }

                                BoosterType current = BoostersManager.getINSTANCE().getPlayerBooster(player);
                                BoosterType selected;
                                try {
                                    selected = BoosterType.valueOf(s);
                                } catch (Exception exception) {
                                    player.closeInventory();
                                    // invalid trail
                                    return;
                                }

                                String displayName;
                                if (current.equals(selected)) {
                                    // deselect
                                    selected = BoosterType.NONE;
                                    displayName = Language.getMsg(player, Messages.NONE_MEANING);
                                } else {
                                    displayName = ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName());
                                }
                                BoosterType finalSelected = selected;
                                Bukkit.getScheduler().runTaskAsynchronously(VipFeatures.plugin,
                                        () -> VipFeatures.databaseAdapter.setSelectedBooster(player.getUniqueId(), finalSelected));
                                player.sendMessage(Language.getMsg(player, Messages.BOOSTER_SELECTED_MESSAGE).replace("{name}", displayName));
                                e.getWhoClicked().closeInventory();
                                BoostersManager.getINSTANCE().setPlayerBooster(player, selected, false);
                                return;
                            }
                        }
                        break;
                }
            }
        }
    }

    @EventHandler
    // Used to apply trails and spells
    public void onBow(EntityShootBowEvent e) {
        if (!(e.getEntity() instanceof Player)) return;

        final Player player = (Player) e.getEntity();
        Arrow arrow = (Arrow) e.getProjectile();
        for (MiniGame m : VipFeatures.getMiniGames()) {
            if (!m.isPlaying(player)) return;
        }
        if (player.hasPermission(Permissions.TRAILS_GUI_PERMISSION) && TrailsManager.getINSTANCE().hasTrails(player)) {
            arrow.setMetadata("trailName", new FixedMetadataValue(VipFeatures.plugin, TrailsManager.getINSTANCE().getPlayerTrails(player).getParticle()));
            TrailsTask.getProjectiles().add(arrow);
        }
        if (player.hasPermission(Permissions.SPELLS_GUI_PERMISSION) && SpellsManager.getINSTANCE().hasSpells(player)) {
            arrow.setMetadata("spellName", new FixedMetadataValue(VipFeatures.plugin, SpellsManager.getINSTANCE().getPlayerSpells(player).toString()));
        }
    }

    @EventHandler
    // Used to execute bow spells
    public void onHit(ProjectileHitEvent e) {
        if (e.getEntity() == null) return;
        if (!(e.getEntity() instanceof Arrow)) return;

        final Arrow arrow = (Arrow) e.getEntity();

        if (arrow.hasMetadata("trailName")) {
            TrailsTask.getProjectiles().remove(arrow);
        }
        if (arrow.hasMetadata("spellName")) {
            if (e.getEntity().getShooter() == null) return;
            if (!(e.getEntity().getShooter() instanceof Player)) return;
            Player player = (Player) e.getEntity().getShooter();

            SpellType spell;
            try {
                spell = SpellType.valueOf(arrow.getMetadata("spellName").get(0).asString());
            } catch (Exception exception){
                // invalid spell
                return;
            }
            spell.execute(arrow.getLocation(), player);
        }
    }

    @EventHandler
    // Used to cache the player preferences
    public void onJoin(PlayerJoinEvent e) {
        final Player player = e.getPlayer();
        Bukkit.getScheduler().runTaskAsynchronously(VipFeatures.plugin, () -> {
            if (player.hasPermission(Permissions.TRAILS_GUI_PERMISSION)) {
                TrailsManager.getINSTANCE().setPlayerTrails(player, VipFeatures.databaseAdapter.getSelectedTrails(player.getUniqueId()));
            }
            if (player.hasPermission(Permissions.SPELLS_GUI_PERMISSION)) {
                SpellsManager.getINSTANCE().setPlayerSpells(player, VipFeatures.databaseAdapter.getSelectedSpells(player.getUniqueId()));
            }
            if (player.hasPermission(Permissions.PARTICLES_GUI_PERMISSION)) {
                ParticleManager.getINSTANCE().setPlayerParticles(player, VipFeatures.databaseAdapter.getSelectedParticles(player.getUniqueId()));
            }
            if (player.hasPermission(Permissions.BOOSTERS_GUI_PERMISSION)) {
                BoostersManager.getINSTANCE().setPlayerBooster(player, VipFeatures.databaseAdapter.getSelectedBooster(player.getUniqueId()), true);
            }
        });
    }

    @EventHandler
    // Stop tasks
    public void onLeave(PlayerQuitEvent e) {
        final Player player = e.getPlayer();
        BukkitTask task = ParticleManager.getINSTANCE().getSpiralRunnables().get(player);
        if (task != null) {
            task.cancel();
            ParticleManager.getINSTANCE().getSpiralRunnables().remove(player);
        }
        if (ParticleManager.getINSTANCE().hasParticles(player)) {
            ParticlesTask.getParticlesOnHead().remove(player);
            ParticleManager.getINSTANCE().getPlayerParticles().remove(player);
        }
        if (SpellsManager.getINSTANCE().hasSpells(player)) {
            SpellsManager.getINSTANCE().getSpellByPlayer().remove(player);
        }
        if (BoostersManager.getINSTANCE().hasBooster(player)) {
            BoostersManager.getINSTANCE().getPlayerBoosters().remove(player);
        }
        if (TrailsManager.getINSTANCE().hasTrails(player)) {
            TrailsManager.getINSTANCE().getTrailByPlayer().remove(player);
        }
    }
}
