package com.andrei1058.vipfeatures.configuration;

public class Messages {

    public static final String PREFIX = "prefix";
    public static final String LANGUAGE_DISPLAY_NAME = "name";

    /**
     * Main GUI language path
     */
    public static final String MAIN_GUI_ITEM_PATH = "perksItem";


    /**
     * {status} placeholder replacements
     */
    public static final String STATUS_REPLACEMENT_NO_PERMISSION = "statusReplacement.noPerm";
    public static final String STATUS_REPLACEMENT_SELECTED = "statusReplacement.selected";
    public static final String STATUS_REPLACEMENT_CLICK_TO_SELECT = "statusReplacement.clickToSelect";

    /**
     * {name} for trail name etc
     */
    public static final String NONE_MEANING = "none";
    public static final String TRAIL_SELECTED_MESSAGE = "trailSelected";
    public static final String SPELL_SELECTED_MESSAGE = "arrowSelected";
    public static final String PARTICLES_SELECTED_MESSAGE = "particlesSelected";
    public static final String BOOSTER_SELECTED_MESSAGE = "boosterSelected";

    public static final String CANT_OPEN_GUI_WHILE_PLAYING = "cantOpenWhilePlaying";
    public static final String CANT_OPEN_GUI_NO_PERM = "cantOpenNoPerm";
    public static final String MINIGAME_BOOSTERS_NOT_SUPPORTED = "noBoostersForMinigame";
}
