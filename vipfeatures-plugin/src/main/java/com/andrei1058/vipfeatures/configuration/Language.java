package com.andrei1058.vipfeatures.configuration;

import com.andrei1058.vipfeatures.VipFeatures;
import com.andrei1058.vipfeatures.customgui.CustomGUI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Language {

    private YamlConfiguration yml;
    private File config;
    private String iso, prefixColor = "", langName = "English";
    private static HashMap<Player, Language> langByPlayer = new HashMap<>();
    private static List<Language> languages = new ArrayList<>();
    private List<CustomGUI> langGUIs = new ArrayList<>();
    private VipFeatures plugin = VipFeatures.plugin;
    private static Language defaultLanguage;

    /**
     * Create a language file with default messages
     *
     * @param iso       Language iso
     * @param addToList make it available to be chosen
     */
    public Language(String iso, boolean addToList) {
        this.iso = iso;
        File d = new File("plugins/" + plugin.getDescription().getName() + "/Languages");
        if (!d.exists()) {
            d.mkdir();
        }
        config = new File(d.toPath() + "/messages_" + iso + ".yml");
        if (!config.exists()) {
            try {
                config.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            plugin.getLogger().info("Creating " + d.toPath() + "/messages_" + iso + ".yml");
        }
        yml = YamlConfiguration.loadConfiguration(config);
        setupLang(this);
        if (addToList) languages.add(this);
    }

    /**
     * Load a language file and add default messages if necessary
     *
     * @param lbj Language instance
     */
    private static void setupLang(Language lbj) {
        YamlConfiguration yml = lbj.yml;
        yml.options().copyDefaults(true);
        switch (lbj.iso) {
            default:
                yml.addDefault(Messages.PREFIX, "&8Perks> ");
                yml.addDefault(Messages.LANGUAGE_DISPLAY_NAME, "English");
                yml.addDefault(Messages.STATUS_REPLACEMENT_NO_PERMISSION, "&c&o(Locked) &3Requires a higher rank!");
                yml.addDefault(Messages.STATUS_REPLACEMENT_CLICK_TO_SELECT, "&eClick to select!");
                yml.addDefault(Messages.STATUS_REPLACEMENT_SELECTED, "&aClick to deselect!");
                yml.addDefault(Messages.TRAIL_SELECTED_MESSAGE, "{prefix}&eTrail selected &a{name}&e!");
                yml.addDefault(Messages.SPELL_SELECTED_MESSAGE, "{prefix}&eSpell selected &a{name}&e!");
                yml.addDefault(Messages.BOOSTER_SELECTED_MESSAGE, "{prefix}&eBooster selected &a{name}&e!");
                yml.addDefault(Messages.PARTICLES_SELECTED_MESSAGE, "{prefix}&eParticles selected &a{name}&e!");
                yml.addDefault(Messages.CANT_OPEN_GUI_WHILE_PLAYING, "{prefix}&eYou can't do this while playing :(");
                yml.addDefault(Messages.CANT_OPEN_GUI_NO_PERM, "{prefix}&cYou can't do that :(");
                yml.addDefault(Messages.MINIGAME_BOOSTERS_NOT_SUPPORTED, "{prefix}&eThis booster won't be applied for &c{minigame}&e.");
                yml.addDefault(Messages.NONE_MEANING, "None");
                break;
        }
        lbj.save();
        lbj.langName = lbj.m(Messages.LANGUAGE_DISPLAY_NAME);
        lbj.prefixColor = ChatColor.translateAlternateColorCodes('&', yml.getString(Messages.PREFIX));
    }

    /**
     * Used at onEnable
     */
    public static void initializeLanguages() {
        new Language("en", false);
        String whatLang = "en";
        for (File f : Objects.requireNonNull(new File("plugins/" + VipFeatures.plugin.getDescription().getName() + "/Languages").listFiles())) {
            if (f.isFile()) {
                if (f.getName().contains("messages_") && f.getName().contains(".yml")) {
                    String lang = f.getName().replace("messages_", "").replace(".yml", "");
                    if (lang.equalsIgnoreCase(VipFeatures.config.getYml().getString("defaultLang"))) {
                        whatLang = f.getName().replace("messages_", "").replace(".yml", "");
                    }
                    Language.setupLang(new Language(lang, true));
                }
            }
        }
        setDefaultLanguage(Language.getLang(whatLang));
    }


    /**
     * Add a new value in the language file
     * This automatically save new changes
     */
    public void set(String path, Object value) {
        yml.set(path, value);
        save();
    }

    /**
     * @return The language's display name
     */
    public String getLangName() {
        return langName;
    }

    /**
     * Save changes in file
     */
    public void save() {
        try {
            yml.save(config);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get a message based on the player's language
     *
     * @param p    Target player.
     * @param path Message path. Use Messages class for messages paths.
     */
    public static String getMsg(Player p, String path) {
        return langByPlayer.containsKey(p) ? langByPlayer.get(p).m(path) : defaultLanguage.m(path);
    }

    /**
     * Get the player's Language
     *
     * @param p Player target
     */
    public static Language getPlayerLanguage(Player p) {
        return langByPlayer.containsKey(p) ? langByPlayer.get(p) : defaultLanguage;
    }

    /**
     * Check if path exists
     *
     * @return True if there is a message with path param.
     */
    public boolean exists(String path) {
        if (yml.get(path) == null) return false;
        return true;
    }

    /**
     * Get a string list based on the player's language
     * Colors are translated
     *
     * @param p    Target player.
     * @param path Message path. Use Messages class for messages paths.
     */
    public static List<String> getList(Player p, String path) {
        return langByPlayer.containsKey(p) ? langByPlayer.get(p).l(path) : defaultLanguage.l(path);
    }

    /**
     * Save a value with path if doesn't exist
     * 10 ticks delay
     */
    public static void saveIfNotExists(String path, Object data) {
        Bukkit.getScheduler().runTaskLater(VipFeatures.plugin, () -> {
            for (Language l : languages) {
                if (l.yml.get(path) == null) {
                    l.set(path, data);
                }
            }
        }, 10L);
    }

    /**
     * Get a message by path.
     * Color are translated.
     * Messages paths are located in Messages class.
     */
    public String m(String path) {
        return ChatColor.translateAlternateColorCodes('&', yml.getString(path).replace("{prefix}", prefixColor));
    }

    /**
     * Get a string list by path.
     * Color are translated.
     * Messages paths are located in Messages class.
     */
    public List<String> l(String path) {
        return yml.getStringList(path).stream().map(s -> s.replace("&", "§")).collect(Collectors.toList());
    }

    /**
     * Get loaded language preferences
     */
    public static HashMap<Player, Language> getLangByPlayer() {
        return langByPlayer;
    }

    /**
     * Check if language with given iso code exists
     */
    public static boolean isLanguageExist(String iso) {
        for (Language l : languages) {
            if (l.iso.equalsIgnoreCase(iso)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get a language instance by iso code
     *
     * @return Returns the default language if query is null
     */
    public static Language getLang(String iso) {
        for (Language l : languages) {
            if (l.iso.equalsIgnoreCase(iso)) {
                return l;
            }
        }
        return defaultLanguage;
    }

    /**
     * Reload a language file.
     * Not recommended with players online.
     */
    public void reload() {
        this.yml = YamlConfiguration.loadConfiguration(config);
    }

    /**
     * Get iso code
     */
    public String getIso() {
        return iso;
    }

    /**
     * Get loaded languages list
     */
    public static List<Language> getLanguages() {
        return languages;
    }

    /**
     * Get loaded GUIs with language support
     */
    public List<CustomGUI> getLangGUIs() {
        return langGUIs;
    }

    /**
     * Get a GUI with language support
     */
    public CustomGUI getGUI(String name) {
        for (CustomGUI sg : langGUIs) {
            if (sg.getName().equalsIgnoreCase(name)) return sg;
        }
        return null;
    }

    /**
     * Set the default language
     * Not recommended with players online
     */
    public static void setDefaultLanguage(Language defaultLanguage) {
        Language.defaultLanguage = defaultLanguage;
    }

    /**
     * Open a GUI if it exists
     *
     * @param name       GUI name
     * @param p          Who to open for
     * @param permission GUI permission. Use Permissions class.
     */
    public void openGUI(String name, Player p, String permission) {
        CustomGUI gui = getGUI(name);
        if (gui == null) return;
        if (permission != null && !permission.isEmpty()) {
            if (!p.hasPermission(permission)) {
                p.sendMessage(getMsg(p, Messages.CANT_OPEN_GUI_NO_PERM));
                p.closeInventory();
                return;
            }
        }
        gui.open(p);
    }
}
