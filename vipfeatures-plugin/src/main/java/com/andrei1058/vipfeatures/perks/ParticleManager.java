package com.andrei1058.vipfeatures.perks;

import com.andrei1058.vipfeatures.VipFeatures;
import com.andrei1058.vipfeatures.api.IVipFeatures;
import com.andrei1058.vipfeatures.api.ParticleType;
import com.andrei1058.vipfeatures.tasks.ParticlesTask;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.HashMap;

import static java.lang.Math.cos;
import static java.lang.StrictMath.sin;

public class ParticleManager implements IVipFeatures.ParticlesUtil {

    private static ParticleManager INSTANCE;

    // preferences list
    private final HashMap<Player, ParticleType> playerParticles = new HashMap<>();

    private final HashMap<Player, BukkitTask> spiralRunnables = new HashMap<>();

    /**
     * Get the preferences list.
     */
    public HashMap<Player, ParticleType> getPlayerParticles() {
        return playerParticles;
    }

    ParticleManager(){}

    /**
     * Check if a player have particle preferences.
     *
     * @param p player.
     * @return true if the player have particle effects.
     */
    public boolean hasParticles(Player p) {
        return getPlayerParticles().containsKey(p);
    }

    /**
     * Get a player particles effects preferences.
     *
     * @param p player.
     * @return the particle type, type none if doesn't have preferences.
     * @see ParticleType
     */
    public ParticleType getPlayerParticles(Player p) {
        if (hasParticles(p)) return getPlayerParticles().get(p);
        return ParticleType.NONE;
    }

    @Override
    public void togglePlayerParticles(Player player, ParticleType particleType) {
        setPlayerParticles(player, particleType);
    }

    /**
     * Save particles effect preferences for a player
     * If the player is already in the preferences list it will be replaced.
     *
     * @param p             player.
     * @param particlesType particle.
     */
    public void setPlayerParticles(Player p, ParticleType particlesType) {
        if (!p.isOnline()) return;
        if (particlesType == ParticleType.NONE) {
            if (hasParticles(p)) {
                getPlayerParticles().remove(p);
                if (getSpiralRunnables().containsKey(p)) {
                    getSpiralRunnables().get(p).cancel();
                    getSpiralRunnables().remove(p);
                }
                ParticlesTask.getParticlesOnHead().remove(p);
            }
            return;
        }
        if (!hasPermission(p, particlesType)) return;
        if (hasParticles(p)) {
            getPlayerParticles().replace(p, particlesType);
        } else {
            getPlayerParticles().put(p, particlesType);
        }
        if (particlesType == ParticleType.SPIRAL) {
            ParticlesTask.getParticlesOnHead().remove(p);
            spiralEffect(p);
        } else {
            if (getSpiralRunnables().containsKey(p)) {
                getSpiralRunnables().get(p).cancel();
                getSpiralRunnables().remove(p);
            }
            if (ParticlesTask.getParticlesOnHead().containsKey(p)) {
                ParticlesTask.getParticlesOnHead().replace(p, particlesType.getParticle());
            } else {
                ParticlesTask.getParticlesOnHead().put(p, particlesType.getParticle());
            }
        }
    }

    /**
     * Start spiral effect for a player.
     *
     * @param player player.
     */
    public void spiralEffect(final Player player) {
        if (getSpiralRunnables().containsKey(player)) {
            getSpiralRunnables().get(player).cancel();
            getSpiralRunnables().remove(player);
        }
        spiralRunnables.put(player,
                new BukkitRunnable() {
                    double phi = 0;

                    public void run() {
                        phi = phi + Math.PI / 8;
                        double x, y, z;

                        Location location1 = player.getLocation();
                        for (double t = 0; t <= 2 * Math.PI; t = t + Math.PI / 16) {
                            for (double i = 0; i <= 1; i = i + 1) {
                                x = 0.4 * (2 * Math.PI - t) * 0.5 * cos(t + phi + i * Math.PI);
                                y = 0.5 * t;
                                z = 0.4 * (2 * Math.PI - t) * 0.5 * sin(t + phi + i * Math.PI);
                                location1.add(x, y, z);
                                VipFeatures.getParticleSupport().spawnRedstoneParticle(location1.getWorld(), (float) location1.getX(), (float) location1.getY(), (float) location1.getZ(), 0, 0, 0, 0, 1);
                                location1.subtract(x, y, z);
                            }

                        }

                        if (phi > 20 * Math.PI) {
                            this.cancel();
                        }
                    }
                }.runTaskTimerAsynchronously(VipFeatures.plugin, 0, 3));
    }

    /**
     * Get active spiral effects.
     */
    public HashMap<Player, BukkitTask> getSpiralRunnables() {
        return spiralRunnables;
    }

    /**
     * Check permission.
     *
     * @param p             player.
     * @param particlesType particles.
     */
    private static boolean hasPermission(Player p, ParticleType particlesType) {
        return p.hasPermission("vipfeatures.*") || p.hasPermission("vipfeatures.particles.*") || p.hasPermission(particlesType.getPermission());
    }

    public static ParticleManager getINSTANCE() {
        if (INSTANCE == null){
            INSTANCE = new ParticleManager();
        }
        return INSTANCE;
    }
}
