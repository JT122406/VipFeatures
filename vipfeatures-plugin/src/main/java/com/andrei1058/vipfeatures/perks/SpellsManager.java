package com.andrei1058.vipfeatures.perks;

import com.andrei1058.vipfeatures.api.IVipFeatures;
import com.andrei1058.vipfeatures.api.SpellType;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class SpellsManager implements IVipFeatures.SpellsUtil {

    private static SpellsManager INSTANCE;

    SpellsManager(){}

    // Contains trails preferences
    private final HashMap<Player, SpellType> spellByPlayer = new HashMap<>();

    /**
     * Get the preferences list.
     */
    public HashMap<Player, SpellType> getSpellByPlayer() {
        return spellByPlayer;
    }

    /**
     * Check if a player have arrow spells preferences.
     *
     * @param p player.
     * @return true if the player have arrow spells.
     */
    public boolean hasSpells(Player p) {
        return getSpellByPlayer().containsKey(p);
    }

    /**
     * Get a player spells preferences.
     *
     * @param p player.
     * @return the spell type, type none if doesn't have preferences.
     * @see SpellType
     */
    public SpellType getPlayerSpells(Player p) {
        if (hasSpells(p)) return getSpellByPlayer().get(p);
        return SpellType.NONE;
    }

    @Override
    public void togglePlayerSpells(Player player, SpellType type) {
        setPlayerSpells(player, type);
    }

    /**
     * Save spells preferences for a player
     * If the player is already in the preferences list it will be replaced.
     *
     * @param p         player.
     * @param spellType spell.
     */
    public void setPlayerSpells(Player p, SpellType spellType) {
        if (!p.isOnline()) return;
        if (spellType == SpellType.NONE) {
            if (hasSpells(p)) getSpellByPlayer().remove(p);
            return;
        }
        if (!hasPermission(p, spellType)) return;
        if (hasSpells(p)) getSpellByPlayer().replace(p, spellType);
        else getSpellByPlayer().put(p, spellType);
    }

    /**
     * Check permission.
     *
     * @param p         player.
     * @param spellType spell.
     */
    private static boolean hasPermission(Player p, SpellType spellType) {
        return p.hasPermission("vipfeatures.*") || p.hasPermission("vipfeatures.spells.*") || p.hasPermission(spellType.getPermission());
    }

    public static SpellsManager getINSTANCE(){
        if (INSTANCE == null){
            INSTANCE = new SpellsManager();
        }
        return INSTANCE;
    }
}
